import React from 'react';
import { Breadcrumb } from 'antd';
import Link from "next/link";
import Styles from '../styles/breadcrumb.module.scss'
import Image from "next/image";
import businessimage from '../public/small4.jpeg'
import {useRouter} from "next/router";

function BreadCrumbComponent() {

    const routes=useRouter();
    const pathName = routes.asPath

    const location = routes.query.Facilities;
    const split = location ? location.replace(/_/g,' ') : '';

    const extraBreadcrumbItems = [
        <Breadcrumb.Item key={pathName}>
                <Link href={pathName}>{split}</Link>
        </Breadcrumb.Item>
    ]

    const breadcrumbItems = [
        <Breadcrumb.Item key="home">
            <Link href="/">Home</Link>
        </Breadcrumb.Item>,
    ].concat(extraBreadcrumbItems);

    return(
        <div className={Styles.menu}>
            <div className={Styles.content}>
                <div className={Styles.content3}>
                    <Breadcrumb separator=">">{breadcrumbItems}</Breadcrumb>
                </div>
                <h2>{split}</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div className={Styles.image}>
                <Image src={businessimage} width={'100%'} height={'100%'} alt={'businessimage'}/>
            </div>
        </div>
    )
}

export default BreadCrumbComponent;