import React from 'react';
import Styles from '../styles/realstory.module.scss'

function RealStoryComponent() {

    return (
        <div className={Styles.main_container}>
            <div className={Styles.container1}>
                <div className={Styles.title}>Lorem Ipsum is simply </div>
                <div style={{height:'300px'}}>
                    <iframe width="100%" height="100%"
                            src="https://www.youtube.com/embed/tgbNymZ7vqY">
                    </iframe>
                </div>
            </div>
            <div className={Styles.container2}>
                <div className={Styles.title1}>
                   <q> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</q>
                </div>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
            </div>

        </div>
    )
}

export default RealStoryComponent;