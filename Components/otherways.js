import React from 'react';
import Styles from '../styles/otherways.module.scss'
import {Card} from "antd";
import {CaretRightOutlined} from "@ant-design/icons";

function OtherWays() {

    return (
        <div className={Styles.main_container}>
            <div className={Styles.card}>
                    <div>
                        <div>
                            <div  className={Styles.learn1}>Lorem Ipsum is not simply random text</div>
                        </div>
                        <div>
                            <div className={Styles.learn}><div>Contrary to popular belief</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                        </div>
                        <div>
                            <div className={Styles.learn}><div>Contrary to popular belief</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                        </div>
                        <div>
                            <div className={Styles.learn}><div>Contrary to popular belief</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default OtherWays;