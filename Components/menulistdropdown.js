import React, {useState} from 'react';
import {CaretRightOutlined, HomeOutlined} from '@ant-design/icons'
import Styles from "../styles/menulistdropdown.module.scss";
import {Card } from "antd";
const { Meta } = Card;
import records from '../Components/dropdownlist.json';
import records2 from '../Components/deposite.json';
import records3 from '../Components/waytobank.json';
import Link from "next/link";


function Menulistdropdown(){

    const [value,setValue] = useState(1)

    return(
        <div className={Styles.main}>
            <div className={Styles.main_container}>
                <div className={Styles.list}>
                    <div onMouseOver={()=>setValue(1)} className={Styles.menu_list} >
                        <span>Savings Account</span>
                        <span><CaretRightOutlined /></span>
                    </div>
                    <div onMouseOver={()=>setValue(2)} className={Styles.menu_list}>
                        <span>Deposites</span>
                        <span><CaretRightOutlined /></span>
                    </div>
                    <div onMouseOver={()=>setValue(3)} className={Styles.menu_list}>
                        <span>Way to Bank</span>
                        <span><CaretRightOutlined /></span>
                    </div>
                </div>
                <div className={Styles.card_list}>
            {

                value == 1 && records.map(data =>(
                    <div>
                    <div className={Styles.main_content}>
                    <Card width={"100%"} hoverable>
                    <Meta
                        title={<div className={Styles.icon}><div><HomeOutlined /></div><div>{data.Label}</div></div>}
                        description={<div className={Styles.icon}>
                            <ul>
                                <li><Link href={data.menu.list1}>{data.menu.list1.replace(/_/g," ")}</Link></li>
                                <li><Link href={data.menu.list2}>{data.menu.list2.replace(/_/g," ")}</Link></li>
                                <li><Link href={data.menu.list3}>{data.menu.list3.replace(/_/g," ")}</Link></li>
                                <li><Link href={data.menu.list4}>{data.menu.list4.replace(/_/g," ")}</Link></li>
                            </ul>
                        </div>}
                    />
                   </Card>
                    </div>
                        <div className={Styles.button}>
                            <div>{data.more}</div>
                       </div>
                    </div>
                ) )
            }
                </div>
                <div className={Styles.card_list}>
                    {

                        value == 2 && records2.map(data =>(
                            <div>
                            <div className={Styles.main_content}>
                                <Card width={"100%"} hoverable>
                                    <Meta
                                        title={<div className={Styles.icon}><div><HomeOutlined /></div><div>{data.Label}</div></div>}
                                        description={<div className={Styles.icon}>
                                            <ul>
                                                <li><Link href={data.menu.list1}>{data.menu.list1.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list2}>{data.menu.list2.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list3}>{data.menu.list3.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list4}>{data.menu.list4.replace(/_/g," ")}</Link></li>
                                            </ul>
                                        </div>}
                                    />
                                </Card>
                            </div>
                                <div className={Styles.button}>
                                    <div>{data.more}</div>
                                </div>
                            </div>
                        ) )
                    }
                </div>
                <div className={Styles.card_list}>
                    {

                        value == 3 && records3.map(data =>(
                            <div>
                            <div className={Styles.main_content}>
                                <Card width={"100%"} hoverable>
                                    <Meta
                                        title={<div className={Styles.icon}><div><HomeOutlined /></div><div>{data.Label}</div></div>}
                                        description={<div className={Styles.icon}>
                                            <ul>
                                                <li><Link href={data.menu.list1}>{data.menu.list1.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list2}>{data.menu.list2.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list3}>{data.menu.list3.replace(/_/g," ")}</Link></li>
                                                <li><Link href={data.menu.list4}>{data.menu.list4.replace(/_/g," ")}</Link></li>
                                            </ul>
                                        </div>}
                                    />
                                </Card>
                            </div>
                                <div className={Styles.button}>
                                    <div>{data.more}</div>
                                </div>
                            </div>
                        ) )
                    }
                </div>
            </div>
        </div>
    )
}

export default Menulistdropdown