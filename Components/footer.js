import React from 'react';
import Image from "next/image";
import img from '../public/Climate.png';
import Styles from '../styles/footer.module.scss'

function Footer() {

    return(
        <div className={Styles.main_container}>
            <div className={Styles.text}>
                <h3>Things you should know</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged
                </p>
                <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
            </div>
            <div className={Styles.container}>
            <div className={Styles.box}>
                <h1>Bank</h1>
                <Image src={img} width={'100%'} />
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
            </div>
            <div className={Styles.box2}>
                <h3>About Bank</h3>
                <p>About Us</p>
                <p>Get in Touch</p>
                <p>Careers</p>
                <p>Site map</p>
                <p>Our Location</p>
            </div>
            <div className={Styles.box2}>
                <h3>Assistance</h3>
                <p>Financial Tips</p>
                <p>Scam & Fraud Protection</p>
                <p>Privacy Policy</p>
                <p>Site disclaimer</p>
                <p>Terms & condition</p>
                <p>Accessibility</p>
            </div>
            </div>
        </div>
    )

}

export default Footer;