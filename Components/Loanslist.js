import React from 'react';
import Styles from '../styles/loanlist.module.scss'
import vegitable from '../public/mother-and-child.jpeg'
import Image from "next/image";
import { Card } from 'antd';
import great from '../public/savings.jpeg'
const { Meta } = Card;

function PersonalLoan(){
    return(
            <div className={Styles.main_container}>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}><Image src={great} alt='carImage' width={'100%'}></Image></div>}
                    >
                        <div className={Styles.titlehead}>
                            <div className={Styles.title}> Lorem Ipsum is simply dummy text of the printing<span className={Styles.subtitle}>and typesetting industry</span></div>
                        </div>
                        <Meta ></Meta>

                    </Card>
                </div>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content2}><Image src={vegitable} alt='childrenImage' width={'100%'} ></Image></div>}
                    >
                        <div className={Styles.title}> Lorem Ipsum is simply dummy text of the printing</div>
                        <Meta
                              description={<p className={Styles.content}>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" </p>} />

                    </Card>
                </div>
            </div>

    )
}

export default PersonalLoan;