import React from 'react';
import Styles from '../styles/haedermenu.module.scss'
import { CaretRightOutlined  } from '@ant-design/icons'
import {Button, Dropdown, Input} from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Menulistdropdown from '../Components/menulistdropdown'
import {useRouter} from "next/router";

function HeaderMenu(){
    const items= [
        {
            label: <Menulistdropdown />,
            key: "1"
        }
    ];

    const headermenu = [
        {
            label:'Lorem 1',
            key:1,
        },
        {
            label:'Lorem 2',
            key:2,
        },
        {
            label:'Lorem 3',
            key:3,
        },
        {
            label:'Lorem 4',
            key:4,
        },
        {
            label:'Lorem 5',
            key:5,
        },
        {
            label:'Lorem 6',
            key:6,
        },
        {
            label:'Lorem 7',
            key:7,
        },

    ]
    const router = useRouter();

    return(
       <div className={Styles.main_container}>
           <div className={Styles.header_main}>
               <div className={Styles.name}>
                   <div className={Styles.headername} onClick={()=>router.push("/")}>Reserver Bank</div>
               </div>
               <div className={Styles.menulist_content}>
                   {headermenu.map(data=>(
                   <Dropdown
                       menu={{ items }}
                   >
                       <a onClick={(e) => e.preventDefault()}>

                           <div className={Styles.menulist} key={data.key}>
                               <div >{data.label}</div>
                               <div className={Styles.arrow}><CaretRightOutlined /></div>
                           </div>
                       </a>
                   </Dropdown>
                   ))}
               </div>

           </div>
           <div className={Styles.container}>
               <div >
                   <button className={Styles.btn} style={{color:'white',border: 'none'}}>Login</button>
               </div>
               <div className={Styles.search_menu}>
                   <div><Input placeholder="I'm lokking for..."  className={Styles.input}  /></div>
                   <div className={Styles.icon}><SearchOutlined /></div>
               </div>
           </div>

       </div>
    )
}

export default HeaderMenu;