import React, {useEffect, useState} from "react";
import {Card, Carousel, Image} from "antd";
import Styles from '../styles/artical.module.scss'
import {CaretRightOutlined,LeftOutlined,RightOutlined } from "@ant-design/icons";
import {throttle} from "lodash";
import ContactList from "./contactlist";
const { Meta } = Card;


function ArticalComponent(){

    const value = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
    };

    const value1 = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2
    };

    const value2 = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const [settings,setSetting] = useState({})
    const [viewportWidth, setViewportWidth] = useState(0);

    useEffect(() => {
        setViewportWidth(window.innerWidth);
        const throttledSetViewPortWidth = throttle(() => setViewportWidth(window.innerWidth), 25);
        window.addEventListener('resize', throttledSetViewPortWidth);
        return () => window.removeEventListener('resize', throttledSetViewPortWidth);
    }, []);

    useEffect(()=>{
       if(viewportWidth <700){
           setSetting(value2)
       }
       else if(viewportWidth <1200){
           setSetting(value1)
       }
       else{
           setSetting(value)

       }

    },[viewportWidth])


    return(
        <div className={Styles.main}>

            <div className={Styles.header}>
                <div className={Styles.head}>
                    Lorem Ipsum is simply dummy
                </div>
                <div className={Styles.container}>
                    <span>View All</span><span><CaretRightOutlined /></span>
                </div>
            </div>
            <div  className={Styles.main_container}>
                <div className={Styles.arrow_prev}>
                    <LeftOutlined />
                </div>
                <div className={Styles.arrow_next}>
                    <RightOutlined />
                </div>
            <Carousel  {...settings} arrows={true}>
                <div className={Styles.container} >
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}>
                            <Image src={"https://media.istockphoto.com/id/1182616138/photo/medicine-doctor-analysis-electronic-medical-record-on-interface-display-dna-digital.jpg?s=170667a&w=0&k=20&c=DpbXL141DuwnHl6JrTmgbyW4lga9MfYk30IqlAo6hxA="} width={"100%"} height={"100%"} alt={"logo"} preview={false}/>
                        </div>}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum</h3>
                                <div className={Styles.content}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>learn more</div>
                                    <div className={Styles.icon}><CaretRightOutlined /></div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}>
                            <Image src={"https://www.conserve-energy-future.com/wp-content/uploads/2015/08/holding-ball-of-earth-ecosystem.jpg"} width={"100%"} height={"100%"} alt={"logo"} preview={false}/>
                        </div>}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum</h3>
                                <div className={Styles.content}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>learn more</div>
                                    <div className={Styles.icon}><CaretRightOutlined /></div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}>
                            <Image src={"https://images.unsplash.com/photo-1522204523234-8729aa6e3d5f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Z3JlZW4lMjBidXNpbmVzc3xlbnwwfHwwfHw%3D&w=1000&q=80"} width={"100%"} height={"100%"} alt={"logo"} preview={false}/>
                        </div>}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum</h3>
                                <div className={Styles.content}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>learn more</div>
                                    <div className={Styles.icon}><CaretRightOutlined /></div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}>
                            <Image src={"https://media-cldnry.s-nbcnews.com/image/upload/newscms/2020_22/1574519/online-shopping-te-inline2-200529.jpg"} width={"100%"} height={"100%"} alt={"logo"} preview={false}/>
                        </div>}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum</h3>
                                <div className={Styles.content}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>learn more</div>
                                    <div className={Styles.icon}><CaretRightOutlined /></div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
            </Carousel>
            </div>
        </div>
    )
}


export default ArticalComponent;
