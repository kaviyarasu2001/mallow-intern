import React from 'react';
import Styles from '../styles/footercontent.module.scss'
import Image from 'next/image'
import facebook from '../public/FB-logo.png'
// import youtube from '../public/youtube.png'
// import twitter from '../public/Twitter.png'
// import instagram from '../public/instagram.png'
// import Image from "next/image";
import {FacebookOutlined,YoutubeOutlined,TwitterOutlined,InstagramOutlined,LinkedinOutlined} from "@ant-design/icons"


function FooterContent(){
    return(
        <div>
            <div className={Styles.main_container}>
                <div className={Styles.container}>
                    <div className={Styles.talk}>
                        <div>Talk to us</div>
                    </div>
                    <div className={Styles.call}>
                        <div>Call 13 14 22</div>
                    </div>
                    <div className={Styles.red_btn}>
                        <div>Find a branch ATM</div>
                    </div>
                    <div  className={Styles.yellow_btn}>
                        <div>Email Us</div>
                    </div>
                    <div className={Styles.fallow}>
                        <div>Follow us</div>
                    </div>
                </div>
                <div className={Styles.social_media}>
                    <div className={Styles.logo}>
                        <Image src={facebook} alt={facebook} />
                        <FacebookOutlined />
                    </div>
                    <div className={Styles.logo}>
                        {/*<Image src={youtube} alt={youtube} />*/}
                        <YoutubeOutlined />
                    </div>
                    <div className={Styles.logo}>
                        {/*<Image src={twitter} alt={twitter} />*/}
                        <TwitterOutlined />
                    </div>
                    <div className={Styles.logo}>
                        {/*<Image src={instagram} alt={instagram} />*/}
                        <InstagramOutlined />
                    </div>
                    <div className={Styles.logo}>
                        {/*<Image src={instagram} alt={instagram} />*/}
                        <LinkedinOutlined />
                    </div>
                </div>

            </div>
        </div>
    )
}

export default FooterContent;