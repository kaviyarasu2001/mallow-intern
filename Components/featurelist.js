import React from 'react';
import Styles from '../styles/featurelist.module.scss'
import { RightOutlined,SaveOutlined,AreaChartOutlined,HomeOutlined,CalculatorOutlined,SafetyCertificateOutlined,DollarOutlined } from '@ant-design/icons'
import Link from "next/link";


function FeatureList(){


    return(
        <div className={Styles.container}>
            <div className={Styles.main_container}>

                <div className={Styles.main}>
                    <div className={Styles.header}>
                        There are many variations of passages
                    </div>
                </div>
                <div>
                    <div className={Styles.grid}>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <SaveOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Banking</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Accounts'}>Accounts</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Credit Cards'}>Credit Cards</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'International'}>International</Link>
                                </div>
                            </div>
                        </div>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <DollarOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Loans</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Home loans'}>Home loans</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Personal loans'}>Personal loans</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Business loans'}>Business loans</Link>
                                </div>
                            </div>
                        </div>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <SafetyCertificateOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Insurance</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Car Insurance'}>Car Insurance</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Home Insurance'}>Home Insurance</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Life Insurance'}>Life Insurance</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={Styles.grid_menu}>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <HomeOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Business</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Accounts'}>Accounts</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Business loans'}>Business loans</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Business Credit Cards'}>Business Credit Cards </Link>
                                </div>
                            </div>
                        </div>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <AreaChartOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Investment</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Tern deposite'}>Tern deposite</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Financial Planning'}>Financial Planning</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Superannuation'}>Superannuation</Link>
                                </div>
                            </div>
                        </div>
                        <div className={Styles.list}>
                            <div className={Styles.image}>
                                <CalculatorOutlined />
                            </div>
                            <div>
                                <div className={Styles.content}>
                                    <div>Calculator</div>
                                    <div className={Styles.icon}>
                                        <RightOutlined />
                                    </div>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Home loan calculators'}>Home loan calculators</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Personal loan calculators'}>Personal loan calculators</Link>
                                </div>
                                <div className={Styles.list_content}>
                                    <Link href={'Savings calculators'}>Savings calculators</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}

export default FeatureList;