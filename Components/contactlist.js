import React from 'react';
import { PhoneOutlined ,MessageOutlined,MailOutlined,EnvironmentOutlined } from '@ant-design/icons';
import Styles from '../styles/contactlist.module.scss'
import {useRouter} from "next/router";
import {Breadcrumb} from "antd";
import Link from "next/link";

function ContactList(){

    const list1 = [
        {
        Name:"Alerts",
        key:'1',
        icon:''
        }];


    return(
        <div className={Styles.main_container}>
            <div className={Styles.alert}>
                {list1.map(list=>(
                    <div key={list.key} className={Styles.content}></div>
                ))}

            </div>

            <div className={Styles.main_contactlist}>
                <div className={Styles.contactlist} >
                    <div className={Styles.icon}><PhoneOutlined /></div>
                    <div className={Styles.content}>Lorem</div>
                </div>
                <div className={Styles.contactlist}>
                    <div className={Styles.icon}><MailOutlined /></div>
                    <div className={Styles.content}>Lorem</div>
                </div>
                <div className={Styles.contactlist}>
                    <div className={Styles.icon}><MessageOutlined /></div>
                    <div className={Styles.content}>Lorem</div>
                </div>
                <div className={Styles.contactlist}>
                        <div className={Styles.icon}><EnvironmentOutlined /></div>
                        <div className={Styles.content}>Lorem</div>
                </div>
            </div>

        </div>
    )
}

export default ContactList;