import React from 'react';
import Styles from '../styles/personalloan.module.scss'
import carImage from '../public/my-trip-cost.webp'
import Image from "next/image";
import { Card } from 'antd';
import child from '../public/plant.webp'

const { Meta } = Card;


function PersonalLoan(){
    return(
        <div>
            <div className={Styles.main_container}>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content1}><Image src={carImage} alt='carImage' width={"100%"}></Image></div>}
                    >
                        <div className={Styles.title}>Lorem Ipsum is simply dummy</div>
                        <Meta title= {<div className={Styles.subtitle}>The printing and typesetting</div>}
                              description={<p className={Styles.content1}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>} />

                    </Card>
                </div>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: '100%' }}
                        cover={<div className={Styles.image_content2}><Image src={child} alt='childrenImage' width={"100%"}></Image></div>}
                    >
                        <div className={Styles.pinktext}>Lorem Ipsum</div>
                        <Meta title= {<div className={Styles.title}>simply dummy text of the printing</div>}
                              description={<p className={Styles.content}>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>} />

                    </Card>
                </div>

            </div>

        </div>
    )
}

export default PersonalLoan;