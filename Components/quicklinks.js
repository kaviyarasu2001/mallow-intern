import React from 'react';
import Styles from "../styles/quicklinks.module.scss";
import {Dropdown} from "antd";
import {CaretRightOutlined} from "@ant-design/icons";
import pointer from '../public/mouse-pointer-cursor.jpg'
import Image from "next/image";

function QuickLinks(){
    const headermenu = [
        {
            label:'Login',
            key:1,
        },
        {
            label:'Activate a card',
            key:3,
        },
        {
            label:'Change your PIN',
            key:4,
        },
        {
            label:'Internet banking help',
            key:5,
        },
        {
            label:'Password help',
            key:6,
        },
    ]
    return(
        <div>
            <div className={Styles.main_container}>
            <div className={Styles.container}>
                <div>
                    <Image src={pointer} alt={pointer} width={24} height={24}></Image>
                </div>
                <div className={Styles.link}>Quick links</div>
            </div>
                    <div className={Styles.menulist_content}>
                        {headermenu.map(data=>(
                            <Dropdown trigger={['click']}>
                                <a onClick={(e) => e.preventDefault()}>
                                    <div className={Styles.menulist}>
                                        <div>{data.label}</div>
                                        <div className={Styles.arrow}><CaretRightOutlined /></div>
                                    </div>
                                </a>
                            </Dropdown>
                        ))}
                    </div>

            </div>

        </div>
    )
}

export default QuickLinks;