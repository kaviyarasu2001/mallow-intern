import React, {useState} from 'react';
import Styles from '../styles/researchtool.module.scss'
import {Card, Menu} from "antd";
const {Meta} = Card
import { HomeOutlined,DashboardOutlined,CarOutlined,PercentageOutlined,BankOutlined,OrderedListOutlined,DollarCircleOutlined } from "@ant-design/icons";

function ResearchTool(){

    const [activeNo,setActiveNo] = useState(2)

    return(
        <div>
            <div className={Styles.container_main}>
                <div className={Styles.container}>
                    <div className={Styles.header}>Research tools</div>
                </div>
                <div className={Styles.main}>
                    <Menu className={Styles.main_tabs}>
                        <Menu.Item
                            className={Styles.tabs1}
                            id={activeNo === 1 ? Styles.activeMenu:''}
                            onClick={()=>setActiveNo(1)}
                        >
                            <div >Calculator</div>
                        </Menu.Item>
                        <Menu.Item
                            className={Styles.tabs2}
                            id={activeNo === 2 ? Styles.activeMenu:''}
                            onClick={()=>setActiveNo(2)}
                        >
                            <div >Guide</div>
                        </Menu.Item>
                        <Menu.Item
                            className={Styles.tabs3}
                            id={activeNo === 3 ? Styles.activeMenu:''}
                            onClick={()=>setActiveNo(3)}
                        >
                            <div>Interest rates</div>
                        </Menu.Item>
                    </Menu>
                </div>
                {activeNo && activeNo === 1 ? (
                    <div className={Styles.card} >
                        <div className={Styles.size}>
                            <Card width={"100%"} height={'100%'} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}><HomeOutlined /></div>}
                                    description={<div className={Styles.icon}>Home loan calculator</div>}
                                />
                            </Card>
                        </div>
                       <div className={Styles.size}>
                           <Card width={"100%"} hoverable >
                               <Meta
                                   title={<div className={Styles.icon}><DashboardOutlined /></div>}
                                   description={<div className={Styles.icon}>Browsing Power calculator</div>}
                               />
                           </Card>
                       </div>
                       <div className={Styles.size}>
                           <Card width={"100%"} hoverable>
                               <Meta
                                   title={<div  className={Styles.icon}><DollarCircleOutlined /></div>}
                                   description={<div className={Styles.icon}>Personal loan calculator</div>}
                               />
                           </Card>
                       </div>
                    </div>
                ):''}
                {activeNo && activeNo === 2 ? (
                    <div className={Styles.card}>
                        <div className={Styles.size}>
                            <Card width={"100%"} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}><OrderedListOutlined /></div>}
                                    description={<div className={Styles.icon}>loan application process</div>}
                                />
                            </Card>
                        </div>
                        <div className={Styles.size}>
                            <Card width={"100%"} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}><CarOutlined /></div>}
                                    description={<div className={Styles.icon}>Buying a car</div>}
                                />
                            </Card>
                        </div>
                        <div className={Styles.size}>
                            <Card width={"100%"} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}><BankOutlined /></div>}
                                    description={<div className={Styles.icon}>Switch to bank</div>}
                                />
                            </Card>
                        </div>
                    </div>
                ):''}
                {activeNo && activeNo === 3 ? (
                    <div className={Styles.card}>
                        <div className={Styles.size}>
                            <Card width={"100%"} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}>7.8<PercentageOutlined /></div>}
                                    description={<div className={Styles.icon}>Home loan Interest Rate</div>}
                                />
                            </Card>
                        </div>
                        <div className={Styles.size}>
                            <Card width={"100%"} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}>10.25<PercentageOutlined /></div>}
                                    description={<div className={Styles.icon}>Account Interest Rate</div>}
                                />
                            </Card>
                        </div>
                        <div className={Styles.size}>
                            <Card width={"100%"} height={'100%'} hoverable>
                                <Meta
                                    title={<div className={Styles.icon}>9.20<PercentageOutlined /></div>}
                                    description={<div className={Styles.icon}>Compare Credit Card</div>}
                                />
                            </Card>
                        </div>
                    </div>
                ):''}
            </div>

        </div>
    )
}

export default ResearchTool;