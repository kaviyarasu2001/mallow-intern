import React, {useEffect, useState} from 'react';
import Styles from '../styles/sightset.module.scss'
import {LeftOutlined, RightOutlined} from "@ant-design/icons";
import { Carousel} from "antd";
import {throttle} from "lodash";




function SightSet(){

    const value = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
    };

    const value1 = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2
    };

    const value2 = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const [settings,setSetting] = useState({})
    const [viewportWidth, setViewportWidth] = useState(0);

    useEffect(() => {
        setViewportWidth(window.innerWidth);
        const throttledSetViewPortWidth = throttle(() => setViewportWidth(window.innerWidth), 25);
        window.addEventListener('resize', throttledSetViewPortWidth);
        return () => window.removeEventListener('resize', throttledSetViewPortWidth);
    }, []);

    useEffect(()=>{
        if(viewportWidth <700){
            setSetting(value2)
        }
        else if(viewportWidth <1200){
            setSetting(value1)
        }
        else{
            setSetting(value)

        }

    },[viewportWidth])

    return(
        <div className={Styles.main_container}>
            <div className={Styles.container}>
                <div className={Styles.head}>LOREM IPSUM</div>
                <div className={Styles.main_text}>Lorem Ipsum is simply dummy text</div>
                <div className={Styles.main}>
                    <div className={Styles.arrow_prev}>
                        <LeftOutlined />
                    </div>
                    <div className={Styles.arrow_next}>
                        <RightOutlined />
                    </div>
                    <Carousel  {...settings} arrows={true}>
                        <div className={Styles.box}>
                            Lorem Ipsum has been the industry's standard dummy text
                        </div>
                        <div className={Styles.box}>
                            Lorem Ipsum has been the industry's standard dummy text
                        </div>
                        <div className={Styles.box}>
                            Lorem Ipsum has been the industry's standard dummy text
                        </div>
                        <div className={Styles.box}>
                            Lorem Ipsum has been the industry's standard dummy text
                        </div>
                    </Carousel>
                </div>
            </div>

        </div>
    )

}

export default SightSet;