import React from 'react';
import dropdowns from '../Components/dropdownlist.json';
import Styles from '../styles/dropdowns.module.scss'

function DropDown(){
    return(
        <div className={Styles.dropdown_head}>
            {dropdowns.map(data=>(
                <div>
                    <h2>{data.Label}</h2>
                </div>
            ))}

        </div>
    )
}

export default DropDown;