import React from 'react';
import Styles from '../styles/supportfile.module.scss'


function SupportFile(){
    return(
        <div className={Styles.main}>
        <div className={Styles.main_container}>
            <div className={Styles.container}>
                <div className={Styles.text}>
                    Lorem Ipsum
                </div>
                <div className={Styles.text}>
                    simply dummy
                </div>
                <div className={Styles.colortext}>
                    printing and typesetting
                </div>
                <div className={Styles.content}>
                    <p>Helping Lorem Ipsum <span>firsts</span> when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                </div>
            </div>
                <div className={Styles.container_video}>
                    <div style={{height:'350px'}}>
                        <iframe width="100%" height="100%"
                                src="https://www.youtube.com/embed/tgbNymZ7vqY">
                        </iframe>
                    </div>
                    <div>
                        <div className={Styles.btn} >Learn about our member's</div>
                    </div>
                </div>
        </div>

        </div>
    )
}

export default SupportFile;