import React from 'react';
import { SearchOutlined } from '@ant-design/icons'
import Styles from '../styles/menulist.module.scss'
import { Input} from 'antd';
const { Search } = Input;
import Menucontents from "../Components/menucontents";

function MenuList(){


    return(
        <div>
            <div className={Styles.main_content}>
                    <div className={Styles.container1}>
                        <Menucontents />
                    </div>
            </div>
            <div className={Styles.container2}>
                <div className={Styles.button}>login</div>
                <div className={Styles.icon}><SearchOutlined /></div>
            </div>

        </div>
    )
}

export default MenuList;