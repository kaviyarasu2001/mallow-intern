import React from 'react'
import Styles from "../styles/framecomponent.module.scss";
import {Card} from "antd";
import {CaretRightOutlined} from "@ant-design/icons";


function FrameComponent(){

    return(
        <div className={Styles.main_container}>
            <div className={Styles.container}>
                <div className={Styles.box}>
                    <Card
                        hoverable
                        style={{ width: '100%',height:'100%' }}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum is simply </h3>
                                <div className={Styles.content}> Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one </div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.box}>
                    <Card
                        hoverable
                        style={{ width: '100%',height:'100%' }}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum </h3>
                                <div className={Styles.content}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature </div>
                            </div>
                            <div>
                                <div className={Styles.learn}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                            </div>
                        </div>
                    </Card>
                </div>

                    <div className={Styles.menu}>
                        <div className={Styles.content1}>
                            <div className={Styles.menu_card1}>
                                <div>
                                    <h3 className={Styles.title}>Lorem Ipsum is simply</h3>
                                    <div className={Styles.content}>looked up one of the more obscure Latin words.Contrary to popular belief, Lorem Ipsum is not simply random text. </div>
                                </div>
                                <div>
                                    <div className={Styles.learn}><div>Learn more</div><div className={Styles.icon} ><CaretRightOutlined /></div> </div>
                                </div>
                            </div>
                        </div>
                        <div className={Styles.content2}>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default FrameComponent;