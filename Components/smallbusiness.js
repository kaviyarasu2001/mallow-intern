import React from 'react';
import Styles from '../styles/smallBusiness.module.scss'
function SmallBusiness(){
    return(
        <div className={Styles.main_container}>
            <div className={Styles.head}> Lorem Ipsum has been the industry's standard dummy</div>
            <div className={Styles.container}>
                <div className={Styles.box}>
                    <div className={Styles.sub_head}>Lorem Ipsum is simply </div>
                    <div className={Styles.content}>
                        A new home—or a new mortgage—might look good on you. Check out our online tools to make your next move easier from start to finish.
                    </div>
                    <div className={Styles.btn}>Buy or refinance</div>
                </div>
                <div className={Styles.box1}>
                    <div className={Styles.sub_head}>Lorem Ipsum is simply </div>
                    <div className={Styles.content}>
                        A new home—or a new mortgage—might look good on you. Check out our online tools to make your next move easier from start to finish.
                    </div>
                    <div className={Styles.btn}>Buy or refinance</div>
                </div>
                <div className={Styles.box}>
                    <div className={Styles.sub_head}>Lorem Ipsum is simply </div>
                    <div className={Styles.content}>
                        A new home—or a new mortgage—might look good on you. Check out our online tools to make your next move easier from start to finish.
                    </div>
                    <div className={Styles.btn}>Buy or refinance</div>
                </div>
            </div>
        </div>
    )
}

export default SmallBusiness;