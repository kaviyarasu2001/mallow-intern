import React from 'react'
import Styles from "../styles/cheque.module.scss";
import {Card} from "antd";
import { CaretRightOutlined } from '@ant-design/icons'
const {Meta} = Card;

function ChequeTransanctionComponent(){
    return(
        <div className={Styles.main_container}>
            <div className={Styles.container}>
                <Card
                    hoverable
                    style={{ width: '100%',height:'100%' }}
                >
                    <div className={Styles.menu_card}>
                        <div>
                            <h3 className={Styles.title}>Lorem Ipsum is simply dummy text of the printing</h3>
                            <p className={Styles.content}> Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old</p>
                        </div>
                        <div>
                            <div className={Styles.learn}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                        </div>
                    </div>

                </Card>
            </div>
            <div className={Styles.container2}>
                <div className={Styles.box}>
                    <Card
                        hoverable
                        style={{ width: '100%',height:'100%' }}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum is simply dummy text of the printing</h3>
                                <p className={Styles.content1}> Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
                            </div>
                            <div>
                                <div className={Styles.learn1}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.box}>
                    <Card
                        hoverable
                        style={{ width: '100%',height:'100%' }}
                    >
                        <div className={Styles.menu_card1}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum is simply dummy text of the printing</h3>
                                <p className={Styles.content1}>Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old</p>
                            </div>
                            <div>
                                <div className={Styles.learn1}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.box}>
                    <Card
                        hoverable
                        style={{ width: '100%',height:'100%' }}
                    >
                        <div className={Styles.menu_card}>
                            <div>
                                <h3 className={Styles.title}>Lorem Ipsum is simply dummy text of the printing</h3>
                                <p className={Styles.content1}>looked up one of the more obscure Latin words.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old</p>
                            </div>
                            <div>
                                <div className={Styles.learn1}><div>Learn more</div><div className={Styles.icon}><CaretRightOutlined /></div> </div>
                            </div>
                        </div>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default ChequeTransanctionComponent;