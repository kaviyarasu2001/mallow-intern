import React from 'react';
import Styles from '../styles/scamalert.module.scss'
import {Card} from "antd";
import Image from "next/image";
import car from '../public/scam-alert.jpeg'

const { Meta } = Card;


function ScamAlerts(){
    return(
        <div className={Styles.main_container}>
                <div className={Styles.container}>
                    <Card
                        hoverable
                        style={{ width: "100%" }}
                        cover={<div className={Styles.image_content}> <Image src={car} alt='carImage' width={"100%"} ></Image></div>}
                    >
                        <Meta title= {<div className={Styles.title}> Latest scam alerts</div>}
                             />
                        <div className={Styles.words}>
                            <div className={Styles.content}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={Styles.container}>
                    <div className={Styles.container1}>
                        <Card style={{ width: "100%",height:"100%" }}  hoverable>
                            <Meta />
                            <div className={Styles.title}>Contact us 24/7</div>
                            <div className={Styles.content}>Lorem Ipsum is simply dummy text of the printing<span> 13 14 22 </span>
                                or if overseas, call<span> +617 4694 9000. </span>
                            </div>
                        </Card>
                    </div>
                    <div className={Styles.container1}>
                        <Card style={{ width: "100%",height:"100" }}  hoverable>
                            <Meta />
                            <div className={Styles.title}>Our locations</div>
                            <div className={Styles.content}>
                                Lorem Ipsum is simply dummy,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout
                            </div>
                        </Card>
                    </div>
                </div>
        </div>
    )
}

export default ScamAlerts;