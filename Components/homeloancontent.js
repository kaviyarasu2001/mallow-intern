import React from 'react';
import Image from "next/image";
import img from '../public/3060214.jpeg'
import {Button} from "antd";
import Styles from '../styles/homeloancontent.module.scss'

function HomeLoanContent(){
    return(
        <div className={Styles.main_container}>
            <div className={Styles.container}>
                <div>
                    <div className={Styles.header}>Contrary to popular <span className={Styles.header_primary}>simply random </span></div>
                    <div className={Styles.cashback}>Simply dummy text </div>
                    <div className={Styles.content}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</div>
                    <Button type='primary' className={Styles.btn} >Lorem Ipsum</Button>
                </div>
            </div>
            <div className={Styles.container1}>
                    <Image src={img}
                           alt={'image'}
                           width={'100%'}
                           height={'100%'}
                    ></Image>
            </div>
        </div>
    )
}

export default HomeLoanContent;