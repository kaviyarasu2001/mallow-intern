import React, {useState} from 'react';
import {
    MailOutlined,
    PhoneOutlined,
    CloseOutlined,
    WechatOutlined,
    EnvironmentOutlined,
    MenuOutlined,
    SearchOutlined
} from '@ant-design/icons';
import { Input,Menu } from 'antd';
import Styles from '../styles/menucontents.module.scss'
import Link from "next/link";
import {useRouter} from "next/router";

const {Search} = Input


function getItem(
    label,
    key,
    icon,
    children,
    type,
){
    return {
        key,
        icon,
        children,
        label,
        type,
    };
}

const items = [
    getItem('Personal', 'sub1', "", [
        getItem('Small Business', 'g1', null,  ),
        getItem('Wealth', 'g5', null, ),
        getItem('Commericial,Corporate & Instutional', 'g3', null, ),
    ]),

    getItem('Checking & Savings', 'sub2',"",[
        getItem('Checking', '5'),
        getItem('Saving', '6'),
        getItem('Banking Service', '7'),
    ]),

    getItem('Credit Cards', 'sub3', "", [
        getItem('Offers', '9'),
        getItem('Online Card', '10'),
        getItem('Banking Service', '11'),
        getItem('More Credit card offers', '12'),
    ]),
    getItem('Loans', 'sub4', "", [
        getItem('Home Loan', '13'),
        getItem('Car Loan', '14'),
        getItem('Agri Loan', '15'),
        getItem('Mortgage Loan', '16'),
    ]),

    getItem('Mortgage', 'sub6', "", [
        getItem('LAP', '21'),
        getItem('Agri loan', '22'),
        getItem('Buy a home', '23'),
    ]),

    getItem('Insurance', 'sub7', "", [
        getItem('Building Insurance', '24'),
        getItem('Life Insurance', '25'),
        getItem('Vehical Insurance', '26'),
    ]),
    getItem('Contact', 'sub8', "", [
        getItem('Call Us', '27',<PhoneOutlined />),
        getItem("Email Us", '28',<MailOutlined />),
        getItem('Live Chat', '29',<WechatOutlined />),
        getItem('Location', '30 ',<EnvironmentOutlined />),

    ]),
    getItem('Login', 'sub9', "", [
    ]),
    getItem('Search', 'sub10', "", [
        getItem(<div className={Styles.search}><Search placeholder={"I'm Looking for..."}  enterButton ></Search></div>,'sub11',""),
    ]),

];

function Menucontents() {

    const [active,setActive] = useState(false)
    const [openKeys, setOpenKeys] = useState(['']);
    const rootSubmenuKeys = ['sub1', 'sub2', 'sub3','sub4','sub5','sub6','sub7','sub8'];
    const router=useRouter();
    const onOpenChange = (keys) => {
        const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(!latestOpenKey) === -2) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };

    return(
        <>

               <div style={{ width: "100vw" }} className={Styles.menu_bar}  >
                   <div   style={{ marginBottom: 16 }} onClick={()=>setActive(!active)} className={Styles.button}>
                       {active ?<div className={Styles.icon}><CloseOutlined /></div>:<div className={Styles.icon}><MenuOutlined /></div>}
                       <div onClick={()=>router.push('/')}>Reserve Bank</div>
                   </div>
                   {/*{active &&*/}
                   {/*    <Menu*/}
                   {/*        openKeys={openKeys}*/}
                   {/*        onOpenChange={onOpenChange}*/}
                   {/*        mode="inline"*/}
                   {/*        style={{ width: "100%" }}*/}
                   {/*    />*/}
                   {/*}*/}
               </div>

        </>
    )
}
export default Menucontents;