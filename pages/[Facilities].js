import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";
import BreadCrumbComponent from "../Components/BreadCrumbComponent";
import ContactList from "../Components/contactlist";
import MenuList from "../Components/menulist";
import HeaderMenu from "../Components/headermenu";
import {throttle} from "lodash";
import Footer from "../Components/footer";
import FooterContent from "../Components/footercontent";
import ChequeTransanctionComponent from "../Components/chequetransanctioncomponent";
import FrameComponent from "../Components/framecomponent";
import RealStoryComponent from "../Components/realstorycomponent";
import OtherWays from "../Components/otherways";
import ArticalComponent from "../Components/articalcomponent";
import Concultion from "../Components/concultion";


function Facilities() {

    const [viewportWidth, setViewportWidth] = useState(0);

    useEffect(() => {
        setViewportWidth(window.innerWidth);
        const throttledSetViewPortWidth = throttle(() => setViewportWidth(window.innerWidth), 25);
        window.addEventListener('resize', throttledSetViewPortWidth);
        return () => window.removeEventListener('resize', throttledSetViewPortWidth);
    }, []);
    const routes=useRouter();
    return(
        <>
            <div>
                {viewportWidth < 720 ? '':<ContactList />}
            </div>
            <div>
                {viewportWidth < 1200 ?  <MenuList /> : <HeaderMenu />}

            </div>
            <div>
                <BreadCrumbComponent routes={routes}/>
            </div>
            <div>
                <ChequeTransanctionComponent />
            </div>
            <div>
                <FrameComponent />
            </div>
            <div>
                <RealStoryComponent />
            </div>
            <div>
                <OtherWays />
            </div>
            <div>
                <ArticalComponent />
            </div>
            <div>
                <Concultion />
            </div>
            {/*<div>*/}
            {/*    <SmallBusiness />*/}
            {/*</div>*/}
            <div>
                <Footer />
            </div>
            <div>
                <FooterContent />
            </div>

        </>
    )
}

export default Facilities;